# frozen_string_literal: true

class PostPolicy < ApplicationPolicy
  # BEGIN
  def create?
    user
  end

  def update?
    user && (user&.admin? || is_user_post_author?)
  end

  def destroy?
    user&.admin?
  end
  # END

  private

  def is_user_post_author?
    record.author_id == user&.id
  end
end
