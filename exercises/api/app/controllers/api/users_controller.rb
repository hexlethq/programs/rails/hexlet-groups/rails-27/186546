# frozen_string_literal: true

module Api
  class UsersController < Api::ApplicationController
    # BEGIN
    before_action :set_user, only: :show

    def index
      respond_with User.all
    end

    def show
      respond_with @user
    end

    private

    def set_user
      @user = User.find(params[:id])
    end
    # END
  end
end
