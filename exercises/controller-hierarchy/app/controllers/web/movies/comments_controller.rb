# frozen_string_literal: true

module Web
  module Movies
    class CommentsController < ApplicationController
      before_action :set_comment, only: %i[edit update destroy]

      def index
        @comments = resource_movie.comments
      end

      def new
        @comment = Comment.new
      end

      def create
        @comment = resource_movie.comments.build(comment_params)

        if @comment.save
          redirect_to movie_comments_path, notice: t('.success')
        else
          render :new, status: unprocessable_entity
        end
      end

      def update
        if @comment.update(comment_params)
          redirect_to movie_comments_path, notice: t('.success')
        else
          render :edit, status: :unprocessable_entity
        end
      end

      def destroy
        if @comment.destroy
          redirect_to movie_comments_path, notice: t('.success')
        else
          redirect_to movie_comments_path, alert: t('.failure')
        end
      end

      private

      def set_comment
        @comment = resource_movie.comments.find(params[:id])
      end

      def comment_params
        params.require(:comment).permit(:body)
      end
    end
  end
end
