# frozen_string_literal: true

module Web
  module Movies
    class ReviewsController < ApplicationController
      before_action :set_review, only: %i[edit update destroy]

      def index
        @reviews = resource_movie.reviews
      end

      def new
        @review = Review.new
      end

      def create
        @review = resource_movie.reviews.build(review_params)

        if @review.save
          redirect_to movie_reviews_path, notice: t('.success')
        else
          render :new, status: unprocessable_entity
        end
      end

      def update
        if @review.update(review_params)
          redirect_to movie_reviews_path, notice: t('.success')
        else
          render :edit, status: :unprocessable_entity
        end
      end

      def destroy
        if @review.destroy
          redirect_to movie_reviews_path, notice: t('.success')
        else
          redirect_to movie_reviews_path, alert: 'Error deleting review'
        end
      end

      private

      def set_review
        @review = resource_movie.reviews.find(params[:id])
      end

      def review_params
        params.require(:review).permit(:body)
      end
    end
  end
end
