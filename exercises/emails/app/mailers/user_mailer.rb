# frozen_string_literal: true

class UserMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.account_activation.subject
  #
  def account_activation
    # BEGIN
    @user_name = params[:user].name
    @user_email = params[:user].email
    @confirmation_token = params[:user].confirmation_token

    mail(
      subject: t('.subject'),
      to: @user_email
    )
    # END
  end
end
