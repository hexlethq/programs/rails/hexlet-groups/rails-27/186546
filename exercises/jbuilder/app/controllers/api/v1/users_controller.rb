# frozen_string_literal: true

module Api
  module V1
    class UsersController < Api::ApplicationController
      # BEGIN
      before_action :set_user, only: :show

      def index
        @users = User.all
      end

      def show; end

      private

      def set_user
        @user = User.find(params[:id])
      end
      # END
    end
  end
end
