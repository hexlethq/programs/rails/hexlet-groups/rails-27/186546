# frozen_string_literal: true

module Api
  module V2
    class UsersController < Api::ApplicationController
      # BEGIN
      before_action :set_user, only: :show

      def index
        @users = User.all
        render json: @users, each_serializer: UserSerializer
      end

      def show
        render json: @user, serializer: UserSerializer
      end

      private

      def set_user
        @user = User.find(params[:id])
      end
      # END
    end
  end
end
