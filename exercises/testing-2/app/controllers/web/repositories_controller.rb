# frozen_string_literal: true

# BEGIN

# END

module Web
  class RepositoriesController < Web::ApplicationController
    def index
      @repositories = Repository.all
    end

    def new
      @repository = Repository.new
    end

    def show
      @repository = Repository.find params[:id]
    end

    def create
      # BEGIN
      client = Octokit::Client.new
      repo_data = client.repo Octokit::Repository.from_url permitted_params[:link]

      @repository = Repository.new({
                                     link: repo_data[:full_name],
                                     owner_name: repo_data[:owner][:login],
                                     repo_name: repo_data[:full_name],
                                     description: repo_data[:description],
                                     default_branch: repo_data[:default_branch],
                                     watchers_count: repo_data[:watchers],
                                     language: repo_data[:language],
                                     repo_created_at: repo_data[:created_at],
                                     repo_updated_at: repo_data[:updated_at]
                                   })

      if @repository.save
        redirect_to @repository, notice: t('success')
      else
        render :new, notice: t('fail')
      end
      # END
    end

    def edit
      @repository = Repository.find params[:id]
    end

    def update
      @repository = Repository.find params[:id]

      if @repository.update(permitted_params)
        redirect_to repositories_path, notice: t('success')
      else
        render :edit, notice: t('fail')
      end
    end

    def destroy
      @repository = Repository.find params[:id]

      if @repository.destroy
        redirect_to repositories_path, notice: t('success')
      else
        redirect_to repositories_path, notice: t('fail')
      end
    end

    private

    def permitted_params
      params.require(:repository).permit(:link)
    end
  end
end
