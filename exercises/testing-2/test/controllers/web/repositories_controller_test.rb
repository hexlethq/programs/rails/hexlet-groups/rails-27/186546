# frozen_string_literal: true

require 'test_helper'

module Web
  class RepositoriesControllerTest < ActionDispatch::IntegrationTest
    # BEGIN
    setup do
      @repo_data_json = load_fixture('/files/response.json')
      @repo_data = JSON.parse(@repo_data_json)
      stub_request(:get, "https://api.github.com/repos/#{@repo_data['full_name']}")
        .to_return(body: @repo_data_json, status: 200, headers: { content_type: 'application/json' })
    end

    test 'should create' do
      post repositories_url, params: { repository: { link: @repo_data['html_url'] } }

      repository = Repository.find_by! link: @repo_data['full_name']
      assert_redirected_to repository_url(repository)
    end
    # END
  end
end
